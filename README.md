# node-problem-detector

Node-problem-detector Helm chart imported from <https://github.com/deliveryhero/helm-charts/tree/master/stable/node-problem-detector>.
We make our own copy due to absence of an official chart for <https://github.com/kubernetes/node-problem-detector>

## Updating

Simply replace `stable/node-problem-detector` folder with updated content from [upstream project](https://github.com/deliveryhero/helm-charts/tree/master/stable/node-problem-detector)

## License

The upstream Helm chart is Copyright © 2023 Delivery Hero under [Apache 2.0 License](https://github.com/deliveryhero/helm-charts/blob/master/README.md#license)